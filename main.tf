provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      Environment = "dev"
      Project     = "cdfi"
    }
  }
}

module "vpc" {
  source = "./modules/vpc"

  vpc_cidr_block             = "10.0.0.0/24"
  public_subnet_cidr_blocks  = ["10.0.0.0/26", "10.0.0.64/26"]
  private_subnet_cidr_blocks = ["10.0.0.128/26", "10.0.0.192/26"]

}

module "mysql_db" {
  source = "./modules/database"

  vpc_id             = module.vpc.vpc_id
  private_subnet_ids = module.vpc.private_subnet_ids
}