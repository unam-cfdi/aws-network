data "aws_availability_zones" "available" {}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "CFDI-vpc"
  }
}

resource "aws_subnet" "public_subnets" {
  count             = length(var.public_subnet_cidr_blocks)
  vpc_id            = aws_vpc.main.id
  cidr_block        = element(var.public_subnet_cidr_blocks, count.index)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "CFDI-public-subnet-${element(data.aws_availability_zones.available.names, count.index)}"
  }
}

resource "aws_subnet" "private_subnets" {
  count             = length(var.private_subnet_cidr_blocks)
  vpc_id            = aws_vpc.main.id
  cidr_block        = element(var.private_subnet_cidr_blocks, count.index)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "CFDI-private-subnet-${element(data.aws_availability_zones.available.names, count.index)}"
  }
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "CFDI-IG"
  }
}

resource "aws_nat_gateway" "my_nat_gateways" {
  count         = length(aws_subnet.public_subnets)
  subnet_id     = aws_subnet.public_subnets[count.index].id
  allocation_id = aws_eip.my_eips[count.index].id

  tags = {
    Name = "CFDI-NG-${count.index}"
  }

}

resource "aws_eip" "my_eips" {
  count  = length(aws_subnet.public_subnets)
  domain = "vpc"

}

resource "aws_route_table" "public_route_tables" {
  count  = length(aws_subnet.public_subnets)
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "cfdi_public_rt-${count.index}"
  }
}

resource "aws_route_table" "private_route_tables" {
  count  = length(aws_subnet.private_subnets)
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "cfdi_private_rt-${count.index}"
  }
}

resource "aws_route" "public_internet_gateway_routes" {
  count                  = length(aws_subnet.public_subnets)
  route_table_id         = aws_route_table.public_route_tables[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my_igw.id
}

resource "aws_route" "private_nat_gateway_routes" {
  count                  = length(aws_subnet.private_subnets)
  route_table_id         = aws_route_table.private_route_tables[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.my_nat_gateways[count.index].id
}

resource "aws_route_table_association" "public_subnet_associations" {
  count          = length(aws_subnet.public_subnets)
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_route_tables[count.index].id
}

resource "aws_route_table_association" "private_subnet_associations" {
  count          = length(aws_subnet.private_subnets)
  subnet_id      = aws_subnet.private_subnets[count.index].id
  route_table_id = aws_route_table.private_route_tables[count.index].id
}