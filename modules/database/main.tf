data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "mssql_server" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  subnet_id              = var.private_subnet_ids[0]
  vpc_security_group_ids = [aws_security_group.mssql_sg.id]
  iam_instance_profile   = aws_iam_instance_profile.my_instance_profile.name

  user_data = <<-EOF
              #!/bin/bash
              sudo su

              # Update the package list and install prerequisites
              apt-get update -y
              apt-get install -y apt-transport-https ca-certificates curl software-properties-common

              # Install Docker
              curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
              add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
              apt-get update -y
              apt-get install -y docker-ce

              # Start Docker service
              systemctl start docker
              systemctl enable docker

              # Pull and run MSSQL Docker container
              docker pull mcr.microsoft.com/mssql/server:2019-latest
              docker run -u root -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=${random_password.sql_password.result}' -p 1433:1433 -v /home/ubuntu:/var/opt/mssql --name mssql -d mcr.microsoft.com/mssql/server:2019-latest
              EOF


  tags = {
    Name    = "CFDI-db"
    DBYype  = "MSSQL"
    Project = "cfdi"
  }
}

resource "aws_security_group" "mssql_sg" {
  name        = "mssql-sg"
  description = "Allow MSSQL traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # You might want to restrict this to your IP range for security reasons
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mssql-sg"
  }
}

#####
# IAM
#####

resource "aws_iam_instance_profile" "my_instance_profile" {
  name = "my-instance-profile"

  role = aws_iam_role.my_ec2_role.name
}

resource "aws_iam_role" "my_ec2_role" {
  name               = "my-ec2-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "ssm_policy_attachment" {
  name       = "ssm-policy-attachment"
  roles      = [aws_iam_role.my_ec2_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_policy" "secret_manager_policy" {
  name        = "SecretManagerAccessPolicy"
  description = "Allows access to a specific secret in AWS Secrets Manager"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect   = "Allow",
      Action   = "secretsmanager:GetSecretValue",
      Resource = aws_secretsmanager_secret.sql_password_secret.arn
    }]
  })
}

resource "aws_iam_role_policy_attachment" "attach_secret_manager_policy" {
  role       = aws_iam_role.my_ec2_role.name
  policy_arn = aws_iam_policy.secret_manager_policy.arn
}

resource "random_password" "sql_password" {
  length           = 20
  special          = true
  override_special = "_%@"
}

resource "aws_secretsmanager_secret" "sql_password_secret" {
  name = "sql-server-password"
}

resource "aws_secretsmanager_secret_version" "sql_password_secret_version" {
  secret_id     = aws_secretsmanager_secret.sql_password_secret.id
  secret_string = random_password.sql_password.result
}
