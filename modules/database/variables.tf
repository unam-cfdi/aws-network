variable "vpc_id" {
  description = "Id of the vpc"
}


variable "private_subnet_ids" {
  description = "IDS for subnet to place EC2"
}
